# @4geit/ngx-swagger-client-service [![npm version](//badge.fury.io/js/@4geit%2Fngx-swagger-client-service.svg)](//badge.fury.io/js/@4geit%2Fngx-swagger-client-service)

---

Swagger client service for angular using the [swagger-client](//www.npmjs.com/package/swagger-client) package.

## Installation

1. A recommended way to install ***@4geit/ngx-swagger-client-service*** is through [npm](//www.npmjs.com/search?q=@4geit/ngx-swagger-client-service) package manager using the following command:

```bash
npm i @4geit/ngx-swagger-client-service --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/ngx-swagger-client-service
```

2. You need to import the `NgxSwaggerClientService` service in the module you want to use it. For instance `app.module.ts` as follows:

```js
import { NgxSwaggerClientService, API_URL } from '@4geit/ngx-swagger-client-service';
```

And you also need to add the `NgxSwaggerClientService` service within the `@NgModule` decorator as part of the `providers` list as well as setting the `API_URL` variable, it requires the URL of the API that exposes the swagger content. This URL is suffixed by `/swagger` and therefore if your URL is for instance `http://localhost:10010/v1`, the URL that will be used to fetch the swagger content will be `http://localhost:10010/v1/swagger`

```js
@NgModule({
  // ...
  providers: [
    // ...
    NgxSwaggerClientService,
    { provide: API_URL, useValue: 'http://localhost:10010/v1' },
    // ...
  ],
  // ...
})
export class AppModule { }
```
