import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxSwaggerClientService } from './ngx-swagger-client.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    NgxSwaggerClientService
  ],
  exports: [
  ]
})
export class NgxSwaggerClientModule { }
