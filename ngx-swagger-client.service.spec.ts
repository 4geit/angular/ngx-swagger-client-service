import { TestBed, inject } from '@angular/core/testing';

import { NgxSwaggerClientService } from './ngx-swagger-client.service';

describe('SwaggerClientService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NgxSwaggerClientService]
    });
  });

  it('should be created', inject([NgxSwaggerClientService], (service: NgxSwaggerClientService) => {
    expect(service).toBeTruthy();
  }));
});
