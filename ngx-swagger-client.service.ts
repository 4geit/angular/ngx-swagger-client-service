import { Inject, Injectable, Optional } from '@angular/core';

import Swagger from 'swagger-client';

import { API_URL } from './variables';

function getUrlTree(apiUrl) {
  const l = document.createElement('a');
  l.href = apiUrl;
  return {
    scheme: l.protocol.slice(0, -1),
    host: `${l.hostname}:${l.port}`,
  };
}
async function resolveSwagger(apiUrl) {
  const { spec } = await Swagger.resolve({
    url: `${apiUrl}/swagger`
  })
  // parse api url and override the host and scheme in the swagger client
  const { host, scheme } = getUrlTree(apiUrl)
  spec.host = host
  spec.schemes = [scheme]
  return spec
}

@Injectable()
export class NgxSwaggerClientService {

  protected apiUrl = 'http://localhost:10010/v1';
  public client: any;

  constructor(
    @Optional()@Inject(API_URL) apiUrl: string
  ) {
    if (apiUrl) { this.apiUrl = apiUrl; }
  }

  async buildClient() {
    const spec = await resolveSwagger(this.apiUrl);
    this.client = await new Swagger({ spec });
  }
  async buildClientWithToken(token: string) {
    this.client = await new Swagger({
      spec: this.client.spec,
      authorizations: {
        AccountSecurity: token
      }
    });
  }

}
